// We import all dependencies
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var router = express.Router();
var http = require('http').Server(app);


require('dotenv').config() // To read the .env file

// You need it to get the body attribute in the request object.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}))


var botkit = require('botkit');

var facebookController = botkit.facebookbot({
  verify_token: process.env.FB_VERIFY_TOKEN,
  access_token: process.env.FB_ACCESS_TOKEN
});

var facebookBot = facebookController.spawn({});
// We create the server
facebookController.setupWebserver("8080",function(err,webserver) {
  facebookController.createWebhookEndpoints(facebookController.webserver, facebookBot, function() {
      console.log('Your facebook bot is connected.');
  });
});


// The bot wait for message here
facebookController.hears(['.*'], 'message_received', function(bot, message){
  const Welcome_message = {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'generic', // The Generic Template Structured Message
        elements: [
	        // Array of products
            {
		      "title": `ELI Miarahaba anao`,
		      "buttons": [
		          {
		            "type": "postback",
		            "payload": "sampana_",
		            "title":"Les parcours"
		          },
		          {
		            "type":"web_url",
		            "url": "http://WWW.eli.mg/ELI_Isoraka_Google_Map",
		            "title":"Pan"
		          },
		          {
		            "type": "web_url",
		            "url": "https://www.eli.mg/eli_class_schedule",
		            "title":"Callendrier"
		          }         
		      ]
	        },
        ]
      }
    }
}

// Send the structured message object instead of a simple string
bot.reply(message, Welcome_message);
});



// Response to the first section
var Sampana = {
    attachment: {
      type: "template",
      payload: {
        template_type: "generic",
        elements: [
          {
            "title": "Anglais/Français",
            "buttons": [
              {
                "type": "postback",
                "title": "Niveau1/Niveau2",
                "payload": "fr_niv1"
              },
              {
                "type": "postback",
                "title": "Niveau3",
                "payload": "fr_niv3"
              }
            ]
          },
          {
            "title": "Italien/Allemand/Espagol",
            "buttons": [
              {
                "type": "postback",
                "title": "Niveau1/Niveau2",
                "payload": "it_niv1"
              },
              {
                "type": "postback",
                "title": "Niveau3",
                "payload": "it_niv3"
              }
            ]
          },
          {
            "title": "chinois",
            "buttons": [
              {
                "type": "postback",
                "title": "Niveau1/Niveau2/Niveau3",
                "payload": "chi_niv1"
              },
              {
                "type": "postback",
                "title": "Niveau4",
                "payload": "chi_niv4"
              }
            ]
          },
          {
            "title": "Spécial Samedi",
            "buttons": [
              {
                "type": "postback",
                "title": "Samedi",
                "payload": "samedi"
              }
            ]
          },
          {
            "title": "Leadership",
            "buttons": [
              {
                "type": "postback",
                "title": "Leadership",
                "payload": "leadership"
              }
            ]
          }
        ]
      }
    }
  }

//We add the schedule file here
var schedule = {
  attachment: {
    type: "image",
    payload: {
      url:"https://github.com/Andrainiony/test-app-mesenger/blob/master/schedule.JPG"
    }
  }

}

// here is the quick reply
var button1 ={
  message:{
  text: "Here is a quick reply!",
  quick_replies:[
    {
      "content_type":"text",
      "title":"Search",
      "payload":"checked",
      "image_url":"http://example.com/img/red.png"
    },
    {
      "content_type":"location"
    }
  ]
}
}


 // All function wait for his postback playload  
facebookController.on('facebook_postback', function (bot, message) {
  if ( message.payload === "sampana_" ) {
    bot.reply( message, Sampana );
  }
  else if (message.payload === "fr_niv1"){
    bot.reply(message, " Durée: 60h \nFrais d'étude: 165 000 Ar" )
  }
  else if (message.payload === "fr_niv3"){
    bot.reply(message, "Durée: 60 Heures\nFrais'détude: 160 000 Ar")
  }
  else if (message.payload === "it_niv1"){
    bot.reply(message, "Durée: 60 Heures\nFrais'détude: 170 000 Ar")
  }
  else if (message.payload === "it_niv3"){
    bot.reply(message, "Durée: 60 Heures\nFrais'détude: 160 000 Ar")
  }
  else if (message.payload === "chi_niv1"){
    bot.reply(message, "Durée: 60 Heures\nFrais'détude: 180 000 Ar")
  }
  else if (message.payload === "chi_niv4"){
    bot.reply(message, "Durée: 60 Heures\nFrais'détude: 170 000 Ar")
  }
  else if (message.payload === "samedi"){
    bot.reply(message, "Durée: 12 séances/niveau\nFrais'détude: 165 000 Ar/niveau")
  }
  else if (message.payload === "leadership"){
    bot.reply(message, "Durée: 50 Heures\nFrais'détude: 180 000 Ar");
    bot.reply(message, button1 )
  }
})
